const parseLinks = require('./parseLinks');

test('validate link transformation', () => {
  const linksObj = [
    {
      'title': 'GitHub Commit',
      'url': 'https://github.com/chalk/ansi-regex/commit/8d1d7cdb586269882c4bdc1b7325d0c58c8f76f9',
    },
    {
      'title': 'GitHub PR',
      'url': 'https://github.com/chalk/ansi-regex/pull/37',
    },
  ];
  const expectedResult = [{
    'url': 'https://github.com/chalk/ansi-regex/commit/8d1d7cdb586269882c4bdc1b7325d0c58c8f76f9',
  }, {
    'url': 'https://github.com/chalk/ansi-regex/pull/37',
  }];
  expect(parseLinks(linksObj)).toStrictEqual(expectedResult);
});
